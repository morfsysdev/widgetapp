import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import reducers from './reducers';
import Main from './views/Main';

const store = createStore(reducers, applyMiddleware(reduxThunk));

class App extends React.Component {
  render() {
    return (
      <Provider store={store} >
        <Main />
      </Provider>
    );
  }
}

export default App;
