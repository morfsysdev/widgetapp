import { ADD_WIDGET, REMOVE_WIDGET } from '../actions/types';

export default function widgetReducers(state={widgets: ['', '', '', '', '', '']}, action){
    switch(action.type){
        case ADD_WIDGET:
            let widgets = [...state.widgets];
            for(let i=0; i<widgets.length; i++) {
                if(widgets[i] === '') {
                    widgets[i] = action.payload;
                    break;
                }
            }
            return { widgets };

        case REMOVE_WIDGET:
            const cCTU = [...state.widgets];
            const iTU = cCTU.findIndex((e)=>{
                return e === action.payload;
            });
            cCTU[iTU] = '';
            return { widgets: cCTU };
        
        default: 
            return state;
    }
}