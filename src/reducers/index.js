import { combineReducers } from 'redux';
import widgetReducers from './widgetReducers';

export default combineReducers({
    widgetReducers
});