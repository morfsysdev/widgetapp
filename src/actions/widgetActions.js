import { ADD_WIDGET, REMOVE_WIDGET } from './types';

export const addWidget = name => dispatch => {
    dispatch({type: ADD_WIDGET, payload: name});
};

export const removeWidget = name => dispatch => {
    dispatch({type: REMOVE_WIDGET, payload: name});
};