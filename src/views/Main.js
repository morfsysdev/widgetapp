import React from 'react';
import UsersActivity from './Widgets/UsersActivity';
import Modal from './Modal';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { addWidget } from '../actions/widgetActions';

import { SERVER_URL } from '../constants/server';

class Main extends React.Component {

    constructor() {
        super();
        this.state = {
            addWidget: false
        }
    }


    drag = (ev) => {
        // console.log(ev.target.id)
        // ev.target.style.opacity = 0;
        ev.dataTransfer.setData("drag", ev.target.id);
    }

    drop = (ev, b) => {
        // console.log(ev.target);
        // ev.preventDefault();
        // console.log(ev.target.id, b);
        if(b !== ev.target.id) {
            return;
        }
        var data = ev.dataTransfer.getData("drag");
        // console.log(ev.target);
        // ev.target.removeChild(document.getElementsByClassName('indWidgetDrop'))
        ev.target.appendChild(document.getElementById(data));
    }

    allowDrop = (ev) => {
        // console.log(event);
        ev.preventDefault();
    }

    getWidget = (widget) => {
        switch(widget) {
            case 'userActivity':
                return <UsersActivity ondragstart={(event) => this.drag(event)} />
            
            default:
                return null;
        }
    }

    render(){

        let { addWidget } = this.state;
        let { widgets } = this.props;

        return (
            <React.Fragment>
                { addWidget ? <Modal closeModal={() => this.setState({addWidget: false})} /> : null }
                <div className="topSection" >
                    <div>Team Dashboard</div>
                    <div>
                        <button className="btn btnAddWidget" onClick={() => this.setState({addWidget: true})}>
                            <img src={SERVER_URL+'assets/images/addwidget.svg'} alt="" />
                            <div>Add Widget</div>
                        </button>
                    </div>
                </div>



                <div className="widgetSection" >
                    {
                        widgets.map((e, i) => {
                            return (
                                <div className="indWidget" key={i} onDrop={(event) => this.drop(event, 'div4')} onDragOver={(event) => this.allowDrop(event) } id="div4" >
                                    { this.getWidget(e) }
                                </div>
                            )
                        })
                    }
                    {/* <div className="indWidget" onDrop={(event) => this.drop(event, 'div1')} onDragOver={(event) => this.allowDrop(event) } id="div1" > 
                        { widgets[0] === 'userActivity' ? <UsersActivity ondragstart={(event) => this.drag(event)} /> : null }
                    </div>
                    <div className="indWidget" onDrop={(event) => this.drop(event, 'div2')} onDragOver={(event) => this.allowDrop(event) } id="div2" >
                        
                    </div>
                    <div className="indWidget" onDrop={(event) => this.drop(event, 'div3')} onDragOver={(event) => this.allowDrop(event) } id="div3" >
                        
                    </div>
                    <div className="indWidget" onDrop={(event) => this.drop(event, 'div4')} onDragOver={(event) => this.allowDrop(event) } id="div4" >
                        
                    </div>
                    <div className="indWidget" onDrop={(event) => this.drop(event, 'div5')} onDragOver={(event) => this.allowDrop(event) } id="div5" >
                        
                    </div>
                    <div className="indWidget" onDrop={(event) => this.drop(event, 'div6')} onDragOver={(event) => this.allowDrop(event) } id="div6" >
                        
                    </div> */}
                </div>
            </React.Fragment>
        )
    }
}

function mapStateToProps(state) {
    return {
        widgets: state.widgetReducers.widgets
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        addWidget
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);