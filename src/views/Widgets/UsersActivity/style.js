export const Styles = {
    widgetDropper: {
        position: 'absolute',
        top: 40,
        right: 10,
        borderRadius: 4,
        backgroundColor: '#fff',
        border: '1px solid #ccc',
        zIndex: 3,
        width: 200,
        overflow: 'hidden'
    },
    widgetOptions: {
        position: 'absolute',
        top: -5,
        right: -520,
        backgroundColor: '#fff',
        borderRadius: 4,
        boxShadow: '2px 2px 10px #ccc',
        width: 500,
        textAlign: 'left'
    },
    wOTitle: {
        borderBottom: '1px solid #ccc',
        padding: 10,
        fontSize: 18,
        fontWeight: 'bold'
    },
    wOFField: {
        width: 300,
        display: 'flex'
    },
    wOField: {
        margin: 10
    },
    wOFLabel: {
        color: '#888',
        marginBottom: 5,
    },
    mIRBot: {
        margin: 10,
        display: 'flex',
        justifyContent: 'flex-end'
    },
    mIRBRight: {
        display: 'flex',
        alignItems: 'center'
    },
    mIRBRInd: {
        marginLeft: 10,
        cursor: 'pointer'
    },
    topHeader: {
        display: 'flex',
        height: 49,
        padding: '0 10px',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottom: '1px solid #ccc',
        fontSize: 14
    },
    rightOptions: {
        display: 'flex',
        alignItems: 'center'
    },
    widgetBot: {
        height: 200,
        overflow: 'auto',
        cursor: 'all-scroll'
    },
    widgetBotInd: {
        height: 40,
        margin: '0 10px',
        display: 'flex',
        alignItems: 'center'
    },
    wBILeft: {
        flex: 1,
        display: 'flex',
        alignItems: 'center'
    },
    wBILImg: {
        marginRight: 10,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    wBILIImg: {
        width: 40,
        height: 40,
        borderRadius: '50%'
    },
    wBILName: {
        fontSize: 12
    },
    wBIRight: {
        flex: 1,
        display: 'flex',
        alignItems: 'center'
    },
    wBIRBar: {
        flex: 1,
        marginRight: 10,
        backgroundColor: '#ccc',
        height: 10,
        zIndex: 1
    },
    wBIRCount: {
        fontSize: 12,
        fontWeight: 'bold'
    }
}