import React from 'react';
import { Styles } from './style';
import axios from 'axios';

import { SERVER_URL } from '../../../constants/server';

export default class UsersActivity extends React.PureComponent {

    state = {
        users: [],
        dropper: false,
        editWidget: false,
        configNumberOfUsers: 5,
        configActivitySorting: 'highest',
        configNumberOfUsersTemp: 5,
        configActivitySortingTemp: 'highest'
    }

    componentDidMount() {
        this.getData();
    }

    getData = async() => {
        const res = await axios.get(SERVER_URL+'assets/data/data.json');
        const { data } = res;
        // console.log(data);
        this.structureUserData(data, (users)=>{
            this.setState({users});
        });
        // this.setState({ users: data.users });
    }

    structureUserData = (data, callback) => {
        let users = [];
        for(let i=0; i<data.users.length; i++) {
            let user = Object.assign({}, data.users[i]);
            user.activity= {
                daily: data.daily[user.id],
                weekly: data.weekly[user.id],
                monthly: data.monthly[user.id]
            };
            users.push(user);
        }
        callback(users);
    }

    getUserList = () => {
        let users = [...this.state.users];

        users.sort((a, b)=>{
            // return a.activity.weekly < b.activity.weekly 
            // ? -1 
            // : ( a.activity.weekly > b.activity.weekly 
            //     ? 1
            //     : 0
            // );
            return (a.activity.weekly - b.activity.weekly) * (this.state.configActivitySorting === 'highest'? -1 : 1);
        });
        return users.slice(0, this.state.configNumberOfUsers);
    }
    
    resetConfigSetting = () => {
        this.setState({configNumberOfUsersTemp: this.state.configNumberOfUsers, configActivitySortingTemp: this.state.configActivitySorting});
    }
    saveConfigSetting = () => {
        this.setState({configNumberOfUsers: this.state.configNumberOfUsersTemp, configActivitySorting: this.state.configActivitySortingTemp, editWidget: false});
    }
    render() {

        let { dropper, editWidget } = this.state;
        // console.log(users);

        return (
            <div className="widgetBg" draggable onDragStart={(event) => this.props.ondragstart(event)} id="drag1" >
                <div style={Styles.topHeader}>
                    <div style={{flex: 1, cursor: 'all-scroll'}} >Users activity</div>
                    <div style={Styles.rightOptions} >
                        <button className="btn btnWidget">
                            <div>Weekly</div>
                            <img src={SERVER_URL+'assets/images/Arrowdowndefault.svg'} alt="" />
                        </button>
                        <button className="btn btnWidget btnRelative" onClick={() => this.setState({dropper: dropper ? false : true})} >
                            <img src={SERVER_URL+'assets/images/Settingsdefault.svg'} alt="" />

                            {/* Dropper */}

                            { dropper ? 
                                <div style={Styles.widgetDropper}>
                                    <ul className="widgetDropper" >
                                        <li 
                                            onClick={(event) => { 
                                                event.stopPropagation(); 
                                                this.setState({dropper: false, editWidget: true }, ()=>{
                                                    this.resetConfigSetting();
                                                }); 
                                            } } 
                                        >Edit Widget</li>
                                        <li>Delete Widget</li>
                                    </ul>
                                </div> : null
                            }

                            {/* Options to modify */}
                            { editWidget ?
                                <div style={Styles.widgetOptions} onClick={(event)=>{event.stopPropagation();}}>
                                    <div style={Styles.wOTitle} >Top Highest percentage of Mobile Time Users</div>
                                    <div style={Styles.wOField} >
                                        <div style={Styles.wOFLabel} >Number of Users</div>
                                        <select style={Styles.wOFField} 
                                        value={this.state.configNumberOfUsersTemp} 
                                        onChange={(event)=>{
                                            this.setState({configNumberOfUsersTemp: event.target.value});
                                        }}>
                                            <option>5</option>
                                            <option>4</option>
                                            <option>3</option>
                                            <option>2</option>
                                            <option>1</option>
                                        </select>
                                    </div>
                                    <div style={Styles.wOField} >
                                        <div style={Styles.wOFLabel} >Activity</div>
                                        <div style={Styles.wOFField} >
                                            <div style={{marginRight: 20}} >
                                                <input checked style={{marginRight: 10}} type="radio" name="activity" value="highest" checked={this.state.configActivitySortingTemp === 'highest'} onChange={(event)=>{this.setState({configActivitySortingTemp: 'highest'})}}/>
                                                <label htmlFor="highest" >Highest</label>
                                            </div>
                                            <div>
                                                <input style={{marginRight: 10}} type="radio" name="activity" value="lowest" checked={this.state.configActivitySortingTemp === 'lowest'} onChange={(event)=>{this.setState({configActivitySortingTemp: 'lowest'})}}/>
                                                <label htmlFor="lowest" >Lowest</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div style={Styles.wOField} >
                                        <div style={Styles.wOFLabel} >Time</div>
                                        <select style={Styles.wOFField} >
                                            <option>Mobile Time</option>
                                        </select>
                                    </div>
                                    <div style={Styles.wOField} >
                                        <div style={Styles.wOFLabel} >Date</div>
                                        <select style={Styles.wOFField} >
                                            <option>Weekly</option>
                                        </select>
                                    </div>

                                    <div style={Styles.mIRBot} >
                                        <div style={Styles.mIRBRight} >
                                            <div style={Styles.mIRBRInd} onClick={() => this.setState({editWidget: false})} >
                                                <img src={SERVER_URL+'assets/images/cancelbuttonwebdefault.svg'} alt="" />
                                            </div>
                                            <div style={Styles.mIRBRInd} 
                                                onClick={()=>{
                                                    this.saveConfigSetting();
                                                }}
                                            >
                                                <img src={SERVER_URL+'assets/images/savebuttonwebdefault.svg'} alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div> : null
                            }
                        </button>
                    </div>
                </div>
                <div style={Styles.widgetBot}>
                    { this.getUserList().length === 0 ? 
                        <div style={{ height: 150, display: 'flex', justifyContent: 'center', alignItems: 'center', fontSize: 15}} >Please wait...</div> :
                        this.getUserList().map(e => {

                            let { name, lastname, activity } = e;

                            return (
                                <div style={Styles.widgetBotInd} key={e.id} >
                                    <div style={Styles.wBILeft} >
                                        <div style={Styles.wBILImg} >
                                            <img style={Styles.wBILIImg} src={SERVER_URL+'assets/images/useravatar1.png'} alt="" />
                                        </div>
                                        <div style={Styles.wBILName} >
                                            { name + ' ' + lastname }
                                        </div>
                                    </div>
                                    <div style={Styles.wBIRight} >
                                        <div style={Styles.wBIRBar} >
                                            <div style={{zIndex: 2, backgroundColor: 'green', height: 10, width: activity.weekly}} ></div>
                                        </div>
                                        <div style={Styles.wBIRCount} >{parseInt(activity.weekly)}%</div>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}