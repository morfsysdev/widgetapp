export const Styles = {
    modalWrapper: {
        position: 'fixed',
        zIndex: 100,
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalWrapperOverlay: {
        postion: 'absolute',
        backgroundColor: '#333',
        opacity: 0.8,
        zIndex: 101,
        width: '100%',
        height: '100%'
    },
    modaBody: {
        position: 'absolute',
        borderRadius: 6,
        width: '75%',
        height: '75%',
        zIndex: 102,
        backgroundColor: '#fff',
        overflow: 'hidden'
    },
    modalTop: {
        height: '10%',
        backgroundColor: '#f1f1f1',
        padding: '0 10px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottom: '1px solid #ccc',
    },
    modalTitle: {
        fontSize: 15,
        fontWeight: 'bold',
    },
    modalClose: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        cursor: 'pointer'
    },
    modalInner: {
        display: 'flex',
        height: '90%'
    },
    mILeft: {
        width: 200,
        borderRight: '1px solid #ccc',
        backgroundColor: '#f1f1f1',
    },
    mILSearch: {
        margin: 10,
        display: 'flex'
    },
    mILSSearch: {
        flex: 1,
        border: '1px solid #ccc',
        borderRadius: 4,
        outline: 'none',
        padding: 10
    },
    mILInd: {
        display: 'flex',
        alignItems: 'center',
        marginBottom: 8,
        cursor: 'pointer'
    },
    activemILInd: {
        display: 'flex',
        alignItems: 'center',
        backgroundColor: '#fff',
        marginBottom: 8,
        cursor: 'pointer'
    },
    mILIImg: {
        margin: '0 10px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    mILIIImg: {
        width: 30,
        height: 30
    },
    mIRight: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        flex: 1
    },
    mIRTop: {
        flex: 1
    },
    mIRTInd: {
        display: 'flex',
        alignItems: 'center',
        padding: '0 10px',
        height: 120,
        borderBottom: '1px solid #ccc'
    },
    mIRTIImg: {
        marginRight: 10
    },
    mIRTIMid: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    mIRTIMTop: {
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 5
    },
    mIRTIMMid: {
        fontSize: 14,
        color: '#888',
        marginBottom: 5,
        flex: 1
    },
    mIRTIMBot: {
        display: 'flex',
        alignItems: 'center'
    },
    mIRTIMBTitle: {
        marginRight: 8,
        fontWeight: 'bold'
    },
    mIRTIMBLabels: {
        display: 'flex',
        alignItems: 'center',
        fontSize: 10,
        fontWeight: 'bold'
    },
    mIRTIMBLInd: {
        marginRight: 5,
        padding: 4,
        backgroundColor: '#f1f1f1',
        borderRadius: 4
    },
    mIRBot: {
        margin: 10,
        display: 'flex',
        justifyContent: 'flex-end'
    },
    mIRBRight: {
        display: 'flex',
        alignItems: 'center'
    },
    mIRBRInd: {
        marginLeft: 10,
        cursor: 'pointer'
    }
}