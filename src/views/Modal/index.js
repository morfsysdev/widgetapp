import React from 'react';
import { Styles } from './style';
import { SERVER_URL } from '../../constants/server';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { addWidget, removeWidget } from '../../actions/widgetActions';

class Modal extends React.Component {

    state = {
        widgetDirectory: true
    }

    anyWidgetExists = () => {
        for(let i=0; i<this.props.widgets.length; i++) {
            if(this.props.widgets[i] !== '') {
                return true;
            }
        }
        return false;
    }

    render() {
        // console.log(this.props.widgets);
        let { widgetDirectory } = this.state;
        let { widgets } = this.props;

        return (
            <div style={Styles.modalWrapper} >
                <div style={Styles.modalWrapperOverlay} ></div>
                <div style={Styles.modaBody} >
                    <div style={Styles.modalTop} >
                        <div style={Styles.modalTitle} >Add a widget</div>
                        <div style={Styles.modalClose} onClick={() => this.props.closeModal()} >
                            <img src={SERVER_URL+'assets/images/closedefault.svg'} alt="" />
                        </div>
                    </div>

                    <div style={Styles.modalInner} >
                        <div style={Styles.mILeft} >
                            <div style={Styles.mILSearch} >
                                <input style={Styles.mILSSearch}  placeholder="Search" />
                            </div>
                            <div style={Styles.mILList} >
                                <div style={ widgetDirectory ? Styles.activemILInd : Styles.mILInd } onClick={() => this.setState({widgetDirectory: true})} >
                                    <div style={Styles.mILIImg} >
                                        <img style={Styles.mILIIImg} src={SERVER_URL+'assets/images/Widgetdirectorydefault.svg'} alt="" />
                                    </div>
                                    <div style={Styles.mILIText} >Widget Directory</div>
                                </div>
                                <div style={ widgetDirectory ? Styles.mILInd : Styles.activemILInd } onClick={() => this.setState({widgetDirectory: false})} >
                                    <div style={Styles.mILIImg} >
                                        <img style={Styles.mILIIImg} src={SERVER_URL+'assets/images/Mywidgetdefault.svg'} alt="" />
                                    </div>
                                    <div style={Styles.mILIText} >My Widget</div>
                                </div>
                            </div>
                        </div>
                        <div style={Styles.mIRight} >

                            { widgetDirectory ? 
                                <div style={Styles.mIRTop} >
                                    <div style={Styles.mIRTInd} >
                                        <div style={Styles.mIRTIImg} >
                                            <img style={Styles.mIRTIIImg} src={SERVER_URL+'assets/images/usersactivitywidgetpicture.png'} alt="" />
                                        </div>

                                        <div style={Styles.mIRTIMid} >
                                            <div style={Styles.mIRTIMTop} >
                                                Users Activity
                                            </div>
                                            <div style={Styles.mIRTIMMid} >
                                                By TimeDoctor
                                            </div>
                                            <div style={Styles.mIRTIMMid} >
                                                Users who worked more or less than their minimum hours required in daily, weekly and monthly.
                                            </div>
                                        </div>

                                        <div style={Styles.mIRTIRight} >
                                            { widgets[0] === 'userActivity' ? 

                                                <button className="btn btnDisabled" disabled >
                                                    <div>Widget added</div>
                                                </button> : 
                                                <button className="btn btnAddWidget" onClick={() => this.props.addWidget('userActivity')} >
                                                    <img src={SERVER_URL+'assets/images/addwidget.svg'} alt="" />
                                                    <div>Add Widget</div>
                                                </button>
                                            }
                                        </div>
                                    </div>
                                </div> :
                                
                                <div style={Styles.mIRTop} >
                                    { !this.anyWidgetExists() ? 
                                        <div style={{padding: 10}} >You haven't added any widgets. Navigate to widget directory to add one.</div> :
                                        widgets[0] === 'userActivity' ?
                                            <div style={Styles.mIRTInd} >
                                                <div style={Styles.mIRTIImg} >
                                                    <img style={Styles.mIRTIIImg} src={SERVER_URL+'assets/images/usersactivitywidgetpicture.png'} alt="" />
                                                </div>

                                                <div style={Styles.mIRTIMid} >
                                                    <div style={Styles.mIRTIMTop} >
                                                        Users worked more than required
                                                    </div>
                                                    <div style={Styles.mIRTIMMid} >
                                                        Users who worked more or less than their minimum hours required in daily, weekly and monthly.
                                                    </div>
                                                    <div style={Styles.mIRTIMBot} >
                                                        <div style={Styles.mIRTIMBTitle} >Variables:</div>
                                                        <div style={Styles.mIRTIMBLabels} >
                                                            <div style={Styles.mIRTIMBLInd} >USERS</div>
                                                            <div style={Styles.mIRTIMBLInd} >WEBSITES</div>
                                                            <div style={Styles.mIRTIMBLInd} >APPS</div>
                                                            <div style={Styles.mIRTIMBLInd} >TIME</div>
                                                            <div style={Styles.mIRTIMBLInd} >DATE</div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style={Styles.mIRTIRight} >
                                                    <button className="btn btnRemoveWidget" onClick={() => this.props.removeWidget('userActivity')} >
                                                        <img src={SERVER_URL+'assets/images/Removewidget.svg'} alt="" />
                                                        <div>Remove Widget</div>
                                                    </button>
                                                </div>
                                            </div> : null
                                    }
                                </div>

                            }

                            <div style={Styles.mIRBot} >
                                <div style={Styles.mIRBRight} >
                                    <div style={Styles.mIRBRInd} onClick={() => this.props.closeModal()} >
                                        <img src={SERVER_URL+'assets/images/cancelbuttonwebdefault.svg'} alt="" />
                                    </div>
                                    {/* <div style={Styles.mIRBRInd} >
                                        <img src={SERVER_URL+'assets/images/savebuttonwebdefault.svg'} alt="" />
                                    </div> */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        widgets: state.widgetReducers.widgets
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        addWidget,
        removeWidget
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Modal);